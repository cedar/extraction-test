package extraction.utils;

import static com.google.api.client.repackaged.com.google.common.base.Strings.nullToEmpty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Entity
 * @author Mihu
 *
 */
public class Entity {
	String type; // entity name
	List<Occurrence> occ = new ArrayList<>();
		
	public Entity(String type) {
		this.type = type;
	}
	
	// getter method
	public String type() { return this.type; }
	public List<Occurrence> occurrences() { return this.occ; }
	
	public String toString() {
		String s = "Entity " + type.toUpperCase() + "\n";
		for (Occurrence o : occ) {
			s += o.toString();
		}
		s += "\n";
		return s;
	}
	
	/**
	 * Build the JSON element of this entity
	 * @return
	 */
	public JSONObject toJSON() {
		
		JSONArray arrOcc = new JSONArray();
		for (Occurrence o : occ) {
			arrOcc.put(o.toJSON());
		}
		
		JSONObject jso = new JSONObject();
		jso.put("type", type);
		jso.put("occurrences", arrOcc);
		
		return jso;
	}
	
	/**
	 * Build the XML element of this entity
	 * @param doc
	 * @return
	 */
	public Element toXML(Document doc) {
		// create the tag <entity>
		Element entityElem = doc.createElement("entity");
		
		// create its child nodes
		Element typeElem = doc.createElement("type");
		typeElem.appendChild(doc.createTextNode(type.toUpperCase()));
		
		// list of occurrences
		Element occElem = doc.createElement("occurrences");
		for (Occurrence o : occ) {
			occElem.appendChild(o.toXML(doc));
		}
		
		// append children
		entityElem.appendChild(typeElem);
		entityElem.appendChild(occElem);
		
		return entityElem;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Entity)) return false;
		Entity e = (Entity)o;
		if (!e.type.equalsIgnoreCase(this.type)) return false; 
		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nullToEmpty(this.type).toLowerCase());
	}
}
