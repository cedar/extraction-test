package extraction.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import com.google.common.base.Joiner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class FileUtils {
	
	// unique ID of a file
	public static String id = "";
	
	// language of a file: two possible choice "fr" and "en"
	public static String lang = "";
	
	// tool used to perform extraction: either "oc" for OpenCalais or "sf" for StanfordNLP
	public static String tool = "";
	
	// extension of the file: "xml" or "json"
	public static String format = "";

	private static String asRelativePath(String... elements) {
		return Joiner.on(File.separatorChar).join(elements);
	}
	
	/**
	 * Name of temp file (used to store intermediate result) <br>
	 * @return <code>data/output/[tool]/tmp/tmp_[tool]_[id]_[lang].format</code>
	 */
	public static String makeFilenameTmp() {
		return asRelativePath("data", "output", tool, "tmp", 
				"tmp_" + tool + "_" + id + "_" + lang + "." + format);
	}
	
	/**
	 * Filename of extraction result <br>
	 * @return <code>data/output/[tool]/[format]/[tool]_[id]_[lang].[format]</code>
	 */
	public static String makeFilenameResult() {
		return asRelativePath("data", "output", tool, format, 
				tool + "_" + id + "_" + lang + "." + format);
	}
	
	/**
	 * Filename of the plain input text <br>
	 * @return <code>data/input/[id]_[lang].txt</code>
	 */
	public static String makeFilenameText() {
		return asRelativePath("data", "input", id + "_" + lang + ".txt");
	}
	
	/**
	 * Filename of the manually annotated text <br><br>
	 * @return <code>data/tagged/[tool]/tagged_[tool]_[id]_[lang].[format]</code>
	 */
	public static String makeFilenameTagged() {
		return asRelativePath("data", "tagged", tool, 
				"tagged_" + tool + "_" + id + "_" + lang + "." + format);
	}
	
	/**
	 * Read input from a file and return its content as a String
	 * @param filename
	 * @return
	 */
	public static String readInput(String filename) {
		Path file = Paths.get(filename);
		Charset charset = Charset.forName("UTF-8");
		String line, content = "";
		try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
		    while ((line = reader.readLine()) != null) {
		        content += line;
		    }
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
		
		return content;
	}
	
	/**
	 * Write a content to a file (in plain text format)
	 * @param content
	 */
	public static void writeResult(String content, String filename) {
		Charset charset = Charset.forName("UTF-8");
		Path p = Paths.get(filename);
		try (BufferedWriter writer = Files.newBufferedWriter(p, charset)) {
			writer.write(content);
			writer.close();
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}
	}
	
	/**
	 * Write the extracted entities (and relations) to file in JSON format
	 * @param entities
	 * @param jsonFile
	 */
	public static void writeResultToJSON(List<Entity> entities, String jsonFile) {
		
		JSONArray arrEntities = new JSONArray();
		for (Entity entity : entities) {
			arrEntities.put(entity.toJSON());
		}
		
		JSONObject jso = new JSONObject().put("entities", arrEntities);
		
		writeResult(jso.toString(1), jsonFile);
	}
	
	/**
	 * Write the extracted entities and relations to XML file
	 * @param entities
	 */
	public static void writeResultToXML(List<Entity> entities, String xmlFile) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			
			// root element <extraction>
			Element rootElem = doc.createElement("extraction");
			doc.appendChild(rootElem);
			
			// tree of all entities
			Element entitiesElem = doc.createElement("entities");
			for (Entity e : entities) {
				entitiesElem.appendChild(e.toXML(doc));
			}
			rootElem.appendChild(entitiesElem);
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(xmlFile));
			transformer.transform(source, result);
			
		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
	}
}
