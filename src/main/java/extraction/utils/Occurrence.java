package extraction.utils;

import static com.google.api.client.repackaged.com.google.common.base.Strings.nullToEmpty;

import java.util.Objects;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.api.client.repackaged.com.google.common.base.Strings;

/**
 * The occurrence of an entity
 * @author Mihu
 *
 */
public class Occurrence {
	String value; // the exact term as detected from the text
	String commonName; // the common name (e.g. Macron is commonly known is Emmanuel Macron
	double confidence; // confidence score (may browse over different knowledge base to compute it)
	long offset; // the start offset of the word
	int length; // the length of the word
	
	public Occurrence(String value, String commonName, double confidence, long offset, int length) {
		this.value = value;
		this.commonName = commonName;
		this.confidence = confidence;
		this.offset = offset;
		this.length = length;
	}
	
	public Occurrence(String value, String commonName, long offset, int length) {
		this.value = value;
		this.commonName = commonName;
		this.offset = offset;
		this.length = length;
		this.confidence = 1.0;
	}
	
	public Occurrence(String value) {
		this.value = value;
		this.commonName = "";
		this.offset = 0;
		this.length = value.length();
		this.confidence = 1.0;
	}
	
	// getter method
	public String value() { return this.value; }
	public String commonName() { return this.commonName; }
	public double confidence() { return this.confidence; }
	public long offset() { return this.offset; }
	public int length() { return this.length; }
	
	/**
	 * Build a JSON representing an occurrence
	 * @return
	 */
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		
		jso.put("value", value);
		jso.put("commonName", commonName);
		jso.put("confidence", confidence);
		jso.put("offset", offset);
		jso.put("length", length);
	
		return jso;
	}
	
	public Element toXML(Document doc) {
		Element occElem = doc.createElement("occurrence");
		
		// value
		Element valueElem = doc.createElement("value");
		valueElem.appendChild(doc.createTextNode(value));
		
		// commonName
		Element commonElem = doc.createElement("commonName");
		commonElem.appendChild(doc.createTextNode(commonName));
		
		// confidence
		Element confElem = doc.createElement("confidence");
		confElem.appendChild(doc.createTextNode(String.valueOf(confidence)));
		
		// offset
		Element offsetElem = doc.createElement("offset");
		offsetElem.appendChild(doc.createTextNode(String.valueOf(offset)));
		
		// length
		Element lengthElem = doc.createElement("length");
		lengthElem.appendChild(doc.createTextNode(String.valueOf(length)));
		
		// add children to occurrence
		occElem.appendChild(valueElem);
		occElem.appendChild(confElem);
		occElem.appendChild(commonElem);
		occElem.appendChild(offsetElem);
		occElem.appendChild(lengthElem);
		
		return occElem;
	}
	
	public String toString() {
		String s = "Occurrence: '" + value 
				+ "', common name = " + commonName + ", confidence = " + confidence 
				+ ", offset = " + offset + ", length = " + length + "\n";
		return s;
	}
	
	@Override
	public boolean equals(Object o) {
		// simple idea: two occurrences are equal if they have the same value
		if (!(o instanceof Occurrence)) return false;
		Occurrence occ = (Occurrence) o;
		if (!occ.value.equalsIgnoreCase(this.value)) return false;
		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nullToEmpty(this.value).toLowerCase());
	}
}
