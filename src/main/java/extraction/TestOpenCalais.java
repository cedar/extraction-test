package extraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import extraction.utils.Entity;
import extraction.utils.FileUtils;
import extraction.utils.Occurrence;

/**
 * Testing extraction with OpenCalais API
 * @author Mihu
 */
public class TestOpenCalais {

	// fixed parameters
	private String url = "https://api.thomsonreuters.com/permid/calais";
	private String tokenKey = "kPH009OVWxaoO68XomAMO1GOpWjh3IQB";
	
	// custom parameters
	private String lang; // language to do tagging
	private String outputFormat ; // output format : json | n3 | rdf
	
	// original content of the file (on which we put entity tags)
	private String text = "";
	
	// set custom parameters
	public void setLanguage(String l) { lang = l; }
	public void setOutputFormat(String format) { outputFormat = format; }
	
	/**
	 * Receives as input a filename, calls APIs to extract entities
	 * @param fileInput input file name
	 * @throws IOException 
	 */
	public void test() {
		// check whether all required parameters are fulfilled
		if (lang == null || outputFormat == null) {
			System.out.println("Please setup all required parameters (text language, output format) before making API calls!");
			System.exit(-1);
		}
		
		// setup the filenames
		FileUtils.tool = "oc";
		if (lang.equalsIgnoreCase("french")) FileUtils.lang = "fr";
		if (lang.equalsIgnoreCase("english")) FileUtils.lang = "en";
		
		// read the input file
		System.out.println("\nReading input text from " + FileUtils.makeFilenameText() + "...");
		text = FileUtils.readInput(FileUtils.makeFilenameText());
		
		// execute the API call
		System.out.println("\nMaking API request to OpenCalais...\n");
		callOpenCalaisAPI();
	}
	
	/**
	 * Calling the API. The procedure is:
	 * <ul>
	 * 	<li>Making API request</li>
	 * 	<li>Write the API response to <code>outFile</code></li>
	 * 	<li>From the format of API response, parse it (here I mostly rely on JSON) 
	 * and write the extraction with custom format (XML) to <code>xmlFile</code></li>
	 * </ul>
	 */
	private void callOpenCalaisAPI() {
		// make call to the API link
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost postRq = new HttpPost(url);

		// add necessary headers (custom)
		postRq.addHeader("x-ag-access-token", tokenKey);
		postRq.addHeader("x-calais-language", lang);
		postRq.addHeader("outputFormat", outputFormat);
		
		// add necessary headers (fixed)
		postRq.addHeader("Content-Type", "text/raw; charset=UTF-8");
		postRq.addHeader("x-calais-contentClass", "news");
	

		// pass body content in the call
		StringEntity entityInput = null;
		try {
			entityInput = new StringEntity(text);
			postRq.setEntity(entityInput);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// execute the call
		HttpResponse response = null;
		try {
			response = httpClient.execute(postRq);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatusLine().getStatusCode());
		}

		// read the response
		String output, result = "";
		BufferedReader br = null;
		
		try {
			br = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
			while ((output = br.readLine()) != null) {
				result += output + "\n";
				System.out.println(output);
			}
			br.close();
		} catch (UnsupportedOperationException | IOException e) {
			e.printStackTrace();
		}
		
		// close the connection
		httpClient.getConnectionManager().shutdown();

		// write the response in file
		System.out.println("\nWrite OpenCalais API response into " + FileUtils.makeFilenameTmp() + "...");
		FileUtils.writeResult(result, FileUtils.makeFilenameTmp());
		
		// parse the output
		System.out.println("\nStart parsing JSON response...");
		parseJSON(result);
	}

	public void parseJSON(String jsonString) {
		// parse JSON from OpenCalais' response
		JSONObject jso = new JSONObject(jsonString);
		
		// remove metadata, prepare to parse the content
		jso.remove("doc");
		Set<String> keys = jso.keySet();
		List<Entity> entities = new ArrayList<>();
		
		for (String k : keys) {
			// parse the regexp of key
			Pattern p = Pattern.compile("http://d.opencalais.com/(.*)/([^/]*)");
			Matcher m = p.matcher(k);
			if (m.matches()) {
				// skip the metadata of returned documents
				if (m.group(1).equals("dochash-1")) continue;
				
				// extract all entities / relations
				JSONObject ob = (JSONObject) jso.get(k);
				
				// define the instance type (entity or relation)
				String str = (String) ob.get("_typeGroup");
				
				if (str.equals("entities")) { // if it is an entity
					// get Entity::type
					String type = ob.getString("_type");
					
					// extract occurrence from JSONObject
					List<Occurrence> occurrences = extractOccurrences(ob);
					
					boolean matched = false;
					for (Entity ent : entities) {
						// if there exists an entity of this type in the list of entities
						// --> add this occurrence to the occurrences of this entity
						if (ent.type().equals(type)) {
							ent.occurrences().addAll(occurrences);
							matched = true;
							break;
						}
					}
					
					// if this is a new entity and has not been added to the list of entities
					if (!matched) {
						Entity entity = new Entity(type);
						entity.occurrences().addAll(occurrences);
						entities.add(entity);
					}
				}
				
				if (str.equals("relations")) { // if it is a relation
					
				}
			}
		}
		
		System.out.println("\nWriting extracted entities to " + FileUtils.makeFilenameResult() + "...");
		
		//for (Entity e : entities) System.out.println(e);
		FileUtils.writeResultToJSON(entities, FileUtils.makeFilenameResult());
	}
	
	/**
	 * Extract an Entity object from a JSON 
	 * (before postprocessing, each entity has only one occurrence, but each occurrence has many mentions)
	 * @param jso
	 * @return the entity if jso is a proper JSONObject, null otherwise
	 */
	private List<Occurrence> extractOccurrences(JSONObject jso) {
		if (!(jso instanceof JSONObject)) return null;
		
		List<Occurrence> occ = new ArrayList<>();
		
		// get Occurrence::commonName ("name" or "commonname" for OpenCalais)
		String commonName = jso.getString("name");
		
		// get Occurrence::confidence
		// only PERSON is provided a confidence score
		double confidence = 0.0;
		try {
			confidence = jso.getJSONObject("confidence").getDouble("aggregate");
		} catch(JSONException e) {
			// in case JSONObject doesn't have confidence object, 
			// catch the exception and let the program continue quietly
		}
		
		// each instance in OpenCalais makes an occurrence
		JSONArray mentions = jso.getJSONArray("instances");
		for (int i = 0; i < mentions.length(); i++) {
			String value = ((JSONObject)mentions.get(i)).getString("exact"); // get Occurrence::value
			long offset = ((JSONObject)mentions.get(i)).getLong("offset"); // get Occurrence::offset
			int length = ((JSONObject)mentions.get(i)).getInt("length"); // get Occurrence::length
			Occurrence occurrence = new Occurrence(value, commonName, confidence, offset, length);
			occ.add(occurrence);
		}
		
		return occ;
	}
}
