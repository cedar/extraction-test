package extraction;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import extraction.utils.Entity;
import extraction.utils.FileUtils;
import extraction.utils.Occurrence;

/**
 * Testing extraction with StanfordNLP
 * @author Mihu
 */
public class TestStanfordNLP {

	/**
	 * Test the tagging of StanfordNLP
	 */
	public void test() {
		// setup filename
		FileUtils.lang = "en";
		FileUtils.tool = "sf";

		// read the input text
		String text = FileUtils.readInput(FileUtils.makeFilenameText());
		Annotation document = new Annotation(text);

		// set the properties
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner"); // all annotators needed for tagging

		// start the pipeline
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		pipeline.annotate(document);

		// write the temporary result to JSON (without post-processing)
		try {
			Writer writer = new FileWriter(FileUtils.makeFilenameTmp());
			pipeline.jsonPrint(document, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// to work with relation later on
		//    	List<RelationMention> relations = document.get(MachineReadingAnnotations.AllRelationMentionsAnnotation.class);
		//    	for (RelationMention r : relations) {
		//    		for (int i = 0; i < r.getArgs().size(); i++) {
		//    			System.out.println(r.getArg(i));
		//    		}
		//    	}

		List<Entity> entities = new ArrayList<>();
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				String type = token.ner();

				// for now, only take into account PERSON, LOCATION, ORGANIZATION (to add temporal details later)
				// remove type.equals("DATE") for now
				if (type.equals("PERSON") || type.equals("LOCATION") || type.equals("ORGANIZATION")) {
					String common = token.word(); // get the word
					long offset = token.beginPosition();
					String value = token.originalText();
					int length = token.endPosition() - token.beginPosition();

					Occurrence occ = new Occurrence(value, common, offset, length);
					Entity entity = new Entity(type);
					entity.occurrences().add(occ);
					entities.add(entity);
				}
			}
		}
		
		List<Entity> processed = mergeOccurrences(mergeEntities(entities));

		// write the extracted result
		FileUtils.writeResultToJSON(processed, FileUtils.makeFilenameResult());
	}
	
	/**
	 * Merge separate occurrences together; e.g. [Emmanuel] [Macron] --> [Emmanuel Macron], 
	 * 
	 * @param entities
	 */
	private List<Entity> mergeOccurrences(List<Entity> entities) {
		List<Entity> merged = new ArrayList<>();

		for (int i = 0; i < entities.size(); i++) {
			Entity _entity = entities.get(i); // current entity
			Entity newEntity = new Entity(_entity.type());

			int j = 0;
			while (j < _entity.occurrences().size()) {
				Occurrence curOcc = _entity.occurrences().get(j);

				long offset = curOcc.offset();
				int length = curOcc.length();
				String value = curOcc.value();
				String commonName = curOcc.commonName();

				int k = 1;
				while (j+k < _entity.occurrences().size()) {
					Occurrence nextOcc = _entity.occurrences().get(j+k);
					if (offset + length + 1 == nextOcc.offset()) {
						value += " " + nextOcc.value();
						commonName += " " + nextOcc.commonName(); 
						length += nextOcc.length() + 1;
					} else {
						break;
					}
					k++;
				}
				j += k;

				// add new occurrence
				Occurrence newOcc = new Occurrence(value, commonName, offset, length);
				newEntity.occurrences().add(newOcc);
			}

			merged.add(newEntity);
		}

		for (Entity e : merged) System.out.println(e);

		return merged;
	}

	/**
	 * Merge all Occurrences of the same Entity together
	 * @param entities
	 */
	private List<Entity> mergeEntities(List<Entity> entities) {
		List<Entity> merged = new ArrayList<>();

		// add in merged list the first element of entities
		merged.add(entities.get(0));

		for (int i = 1; i < entities.size(); i++) {
			Entity ent = entities.get(i);

			// if merged contains an entity of the same type (only compare "type" to judge equality) 
			// --> merge the occurrences
			if (merged.contains(ent)) {
				int k = merged.indexOf(ent);
				merged.get(k).occurrences().addAll(ent.occurrences());
			} else { // otherwise, add to merged list
				merged.add(ent);
			}
		}

		return merged;
	}
}
