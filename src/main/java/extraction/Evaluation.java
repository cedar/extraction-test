package extraction;

import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import extraction.utils.Entity;
import extraction.utils.FileUtils;
import extraction.utils.Occurrence;

/**
 * The manually annotated text is <b>always</b> 
 * @author Mihu
 *
 */
public class Evaluation {

	private double recall;
	private double precision;
	private double fMeasure;

	public void evaluate() {
		if (FileUtils.format.equalsIgnoreCase("xml")) 
			evaluateXML(FileUtils.makeFilenameResult(), FileUtils.makeFilenameTagged());

		if (FileUtils.format.equalsIgnoreCase("json"))
			evaluateJSON(FileUtils.makeFilenameResult(), FileUtils.makeFilenameTagged());
	}

	private void evaluateJSON(String testFile, String labeledFile) {
		// parse two json
		JSONArray jsL = new JSONObject(FileUtils.readInput(labeledFile)).getJSONArray("entities");
		JSONArray jsT = new JSONObject(FileUtils.readInput(testFile)).getJSONArray("entities");

		// extract the entitie (simplified)
		List<Entity> eL = extractEntities(jsL);
		List<Entity> eT = extractEntities(jsT);
		
		// compute accuracy measure
		compareResult(eT, eL);
	}
	
	/**
	 * Will only compare entity name and word, 
	 * discard all indicators of relevance, offsets, etc.
	 * 
	 * Compute recall, precision, F-measure
	 * @param labeledFile xml file that is manually labeled 
	 * @param testFile xml file that is resulting from a test (OpenCalais, StanfordNLP, etc.)
	 */
	private void evaluateXML(String testFile, String labeledFile) {
		// parse two xml
		Document docL = parseXML(labeledFile);
		Document docT = parseXML(testFile);

		// if error occurs --> halt the program
		if (docL == null || docT == null) System.exit(-1);

		// extract <entity> tags
		NodeList elemsL = docL.getElementsByTagName("entity");
		NodeList elemsT = docT.getElementsByTagName("entity");

		// list of extracted entities
		List<Entity> entL = extractEntities(elemsL);
		List<Entity> entT = extractEntities(elemsT);

		// compute accuracy measure
		compareResult(entT, entL);
	}

	private void compareResult(List<Entity> test, List<Entity> labeled) {
		// start to compute accuracy measure
		int QaD = nbOccurrences(test); // Qa(D): result from algorithm A to resolve query Q applied to dataset D
		int QsD = nbOccurrences(labeled); // Qs(D): golden standard expected to obtain on query Q applied to dataset D
		
		// compute QaD ^ QsD
		int hit = 0;
		for (Entity l : labeled) {
			for (Entity t : test) {
				if (!t.type().equalsIgnoreCase(l.type())) continue;
				// if of the same entity
				for (Occurrence occ : l.occurrences()) {
					if (t.occurrences().contains(occ)) {
						hit++;
						System.out.println("Matched: " + occ.value());
					}
				}
			}
		}

		recall = ((double)hit) / ((double)QsD);
		precision = ((double)hit)/ ((double)QaD);
		fMeasure = 2 * precision * recall / (precision + recall);

		// some pretty decimal formats
		DecimalFormat df = new DecimalFormat("#.###");
		df.setRoundingMode(RoundingMode.CEILING);

		System.out.println("\n****** Evaluation result ******");
		System.out.println("- precision: " + df.format(precision));
		System.out.println("- recall: " + df.format(recall));
		System.out.println("- F-measure: " + df.format(fMeasure));
		System.out.println("*******************************\n");

		System.out.println("The evaluation is done on 2 text sets: ");
		System.out.println("- Manually labeled text: " + FileUtils.makeFilenameTagged());
		System.out.println("- Automatically extracted text: " + FileUtils.makeFilenameResult());
	}

	/**
	 * Extract entities from JSONObject
	 * @param jso
	 * @return
	 */
	private List<Entity> extractEntities(JSONArray jsa) {
		List<Entity> entities = new ArrayList<>();

		for (int i = 0; i < jsa.length(); i++) {
			JSONObject jso = jsa.getJSONObject(i);

			// get the type
			String type = jso.getString("type");
			Entity entity = new Entity(type);

			// get all the occurrences
			JSONArray jocc = jso.getJSONArray("occurrences");
			for (int j = 0 ; j < jocc.length(); j++) {
				String value = jocc.getJSONObject(j).getString("value");
				entity.occurrences().add(new Occurrence(value));
			}
			entities.add(entity);
		}

		return entities;
	}
	
	private int nbOccurrences(List<Entity> entities) {
		int total = 0;
		for (Entity e : entities) {
			total += e.occurrences().size();
		}
		return total;
	}

	/**
	 * Extract entities from XML format
	 * @param elems
	 * @return
	 */
	private List<Entity> extractEntities(NodeList elems) {
		List<Entity> entities = new ArrayList<>();
		for (int i = 0; i < elems.getLength(); i++) {
			String type, value;
			Element details = (Element) elems.item(i).getChildNodes();
			type = details.getElementsByTagName("type").item(0).getTextContent(); // extract entity name
			value = details.getElementsByTagName("value").item(0).getTextContent(); // extract entity word

			Entity entity = new Entity(type);
			entity.occurrences().add(new Occurrence(value));
			entities.add(entity);
		}

		return entities;
	}

	/**
	 * Parse the XML file and return the corresponding DOM document element
	 * @param file XML file to parse
	 * @return
	 */
	private Document parseXML(String file) {
		Document doc = null;

		// parse the XML file
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(new File(file));
			doc.getDocumentElement().normalize();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		return doc;
	}
	
	/**
	 * A simplified version of Entity class, used only to compare
	 * @author Mihu
	 *
	 */
	class SimpleEntity {
		
	}
}
