package inria.ie;

import extraction.Evaluation;
import extraction.TestOpenCalais;
import extraction.TestStanfordNLP;
import extraction.utils.FileUtils;

/**
 * App for main method
 * @author Mihu
 *
 */
public class App 
{
	/**
	 * Test Entity tagging with OpenCalais API
	 */
	static void testOpenCalais() {
		TestOpenCalais oc = new TestOpenCalais();
		
		// set required parameters
		oc.setLanguage("English");
		oc.setOutputFormat("application/json");
		
		// choose the file id and result format (that is, format of the extraction file after post-processing)
		FileUtils.id = "04";
		FileUtils.format = "json";
		
		// call API test
		oc.test();
		
		// evaluate the result
		evaluate();
	}
	
	/**
	 * Test Entity tagging with StanfordAPI (to complete)
	 */
	static void testStanfordNLP() {
		TestStanfordNLP sf = new TestStanfordNLP();
		
		// choose the file id and result format (that is, format of the extraction file after post-processing)
		FileUtils.id = "04";
		FileUtils.format = "json";
		
		// let it run
		sf.test();
		
		// evaluate the result
		evaluate();
	}
	
	/**
	 * Evaluate the result
	 * @param labeled filename of manually labeled text
	 * @param test filename of automatically extracted text
	 */
	static void evaluate() {
		Evaluation eval = new Evaluation();
		eval.evaluate();
	}
	
	public static void main(String[] args)
	{
		//testOpenCalais();
		testStanfordNLP();
	}
}
